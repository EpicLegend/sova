'use strict';
//= ../node_modules/lazysizes/lazysizes.js

//= ../node_modules/jquery/dist/jquery.js

//= ../node_modules/popper.js/dist/umd/popper.js

//= ../node_modules/bootstrap/js/dist/util.js
//= ../node_modules/bootstrap/js/dist/alert.js
//= ../node_modules/bootstrap/js/dist/button.js
//= ../node_modules/bootstrap/js/dist/carousel.js
//= ../node_modules/bootstrap/js/dist/collapse.js
//= ../node_modules/bootstrap/js/dist/dropdown.js
//= ../node_modules/bootstrap/js/dist/modal.js
//= ../node_modules/bootstrap/js/dist/tooltip.js
//= ../node_modules/bootstrap/js/dist/popover.js
//= ../node_modules/bootstrap/js/dist/scrollspy.js
//= ../node_modules/bootstrap/js/dist/tab.js
//= ../node_modules/bootstrap/js/dist/toast.js

//= ../node_modules/@fancyapps/fancybox/dist/jquery.fancybox.min.js

//= library/slick.js



//= components/slider__slick.js


$(document).ready(function () {


	/* START preloader*/
	(function () {
		if ( Date.now() - preloader.timerStart >= preloader.duration  ) {
			$("#preloader-cube").animate({"opacity" : "0"}, preloader.duration / 2, function () {
				$("#preloader-cube").css("display", "none");
			});
		} else {
			setTimeout(function () {
				$("#preloader-cube").animate({"opacity" : "0"}, preloader.duration / 2, function () {
					$("#preloader-cube").css("display", "none");
				});
			}, preloader.duration / 1.7)
		}
	})();
	/* END preloader */

	/* START Открытие меню */
	$(".btn__menu").on("click", function () {
		$(this).toggleClass("active");

		if ( $(this).hasClass("active") ) {
			$(".navigation__content").addClass("active");
		} else {
			$(".navigation__content").removeClass("active");
		}
	});

	$(".btn_header").on("click", function () {
		if( $(".navigation__content").hasClass("active") ) {
			$(".navigation__content").removeClass("active");
			$(".btn__menu").removeClass("active");
			$("#burger").removeClass("active");
		}
	});
	/* END откртие меню*/

	/* START красиво для hover в navigation */
	$(".navigation ul li a").hover(function () {
		var width = $(this).width();

		var elem = $(this);
		var offset = elem.offset().left - elem.parent().parent().offset().left;
		offset += parseInt( elem.css("padding-left") );

		$(".navigation__line").width( width );
		$(".navigation__line").css("left", offset);
		$(".navigation__line").css("bottom", "-6px");
	}, function () {
		// Забрать красоту! ВСЮ!
		$(".navigation__line").css("bottom", "-500%");
	});
	/* END красиво для hover в navigation */

	if ( $(window).scrollTop() > 0 ) {
        //$("header").addClass("bg-dark");
    }
	$(window).on("scroll", function () {
		if ( $(window).scrollTop() > 0 ) {
			//$("header").addClass("bg-dark");
		}  else {
			//$("header").removeClass("bg-dark");
		}

		/* START кнопка вверх */
		if ( $(window).scrollTop() > 100 ) {
			$(".btn-to-top").addClass("btn-to-top_active");
		} else {
			$(".btn-to-top").removeClass("btn-to-top_active");
		}

	});
	$('.btn-to-top').click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 400);
        return false;
    });
	/* END кнопка вверх */





	$( window ).resize(function() {
		if ( $(window).width()  > 991 ) {
			$(".navigation__content").removeClass("active");
			$(".btn__menu").removeClass("active");
			$(".btn__menu svg").removeClass("active");
		}
	});


	$(".articles__footer__add").click(function (e) {
		e.preventDefault();
		$(this).parent().find(".articles__footer__comment").toggle( "slow", function() {
			
		});
	});





});


